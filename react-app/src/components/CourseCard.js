
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';


export default function CourseCard({courseProp}){


	const {_id, name, description, price} = courseProp;



	// Use the stae hook for this component to be able to store its state.
	// states are used to keep track of information related to individual components
		//const [getter, setter] = useState(initialGetterValue);

	const [enrollees, setEnrollees] = useState(0);
	const [seats, setSeats] = useState(30);

	const [isDisabled, setIsDisabled] = useState(false);

	useEffect(()=>{
		if (seats === 0 ){
			setIsDisabled(true);
		}
	},[seats])

		// setEnrollees(1);
/*	function enroll(){
		
		if(seats<=0){
			alert("no more seats")
		}
		else{
			setEnrollees(enrollees + 1);
		setSeats(seats - 1)
		}
	}*/
	const {user} = useContext(UserContext);


	function enroll(){
		
		if(seats>1 && enrollees<29){
			setEnrollees(enrollees + 1);
			setSeats(seats - 1)
			

		}
		else{
			alert("no more seats")
			setEnrollees(enrollees + 1);
			setSeats(seats - 1)
		}
	}

	// Define a 'useEffect' hook to have the "CourseCard" component do perform a certain task.

	//This will run automatically.

	// Syntax:
		// useEffect(sideEffect/function,[dependencies]);

	//sideEffect/function - it will run on the first load and will reload dependin

	return(
			<Row className = "mt-5">
				<Col>
					<Card>
					     
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>{price}</Card.Text>
					        <Card.Subtitle>Enrollees:</Card.Subtitle>
					        <Card.Text>{enrollees}</Card.Text>
					        <Card.Subtitle>Available seats:</Card.Subtitle>
					        <Card.Text>{seats}</Card.Text>

					        {
					        	user ?
					        	<Button as = {Link} to = {`/course/${_id}`} disabled={isDisabled}>See more details</Button>

					        	:

					        	<Button as = {Link} to = "/login" >Login</Button>

					        }


					        
					      </Card.Body>
					 </Card>
				</Col>
			</Row>
		)
}