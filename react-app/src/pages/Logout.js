import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';



export default function Logout(){

		// localStorage.clear();

	const {unSetUser, setUser} = useContext(UserContext);

useEffect(()=>{
unSetUser();
setUser(localStorage.getItem('email'));

},[unSetUser, setUser])
	

	return(

		<Navigate to = "/login"/>

		)
}
